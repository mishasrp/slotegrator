<?php

use yii\db\Migration;

/**
 * Class m220704_155608_insert_user_row
 */
class m220704_155608_insert_user_row extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        Yii::$app->db->createCommand()->batchInsert('user', ['email', 'auth_key', 'password_hash'], [
            [
                'email' => 'test@test.ua',
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash('12345')
            ]
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220704_155608_insert_user_row cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220704_155608_insert_user_row cannot be reverted.\n";

        return false;
    }
    */
}
