<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_prize}}`.
 */
class m220704_122829_create_user_prize_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_prize}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'prize_id' => $this->integer()->notNull(),
            'prize_item_id' => $this->integer()->Null(),
            'value' => $this->integer()->Null(),
            'issued' => $this->boolean()->notNull()->defaultValue(0)
        ]);


        $this->createIndex(
            '{{%user_prize_user_id_idx}}',
            '{{%user_prize}}',
            'user_id'
        );
        $this->createIndex(
            '{{%user_prize_prize_id_idx}}',
            '{{%user_prize}}',
            'prize_id'
        );
        $this->createIndex(
            '{{%user_prize_prize_item_id_idx}}',
            '{{%user_prize}}',
            'prize_item_id'
        );

        $this->addForeignKey(
            '{{%fk_user_prize_user_id_idx}}',
            '{{%user_prize}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%fk_user_prize_prize_id_idx}}',
            '{{%user_prize}}',
            'prize_id',
            '{{%prizes}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%fk_user_prize_prize_items_id_idx}}',
            '{{%user_prize}}',
            'prize_id',
            '{{%prize_items}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_prize}}');
    }
}
