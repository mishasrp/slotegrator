<?php

use yii\db\Migration;

/**
 * Class m220704_105135_insert_prizes_rows
 */
class m220704_105135_insert_prizes_rows extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Yii::$app->db->createCommand()->batchInsert('prizes', ['prize_name', 'prize_attribute'], [
            [
                'prize_name' => 'Money',
                'prize_attribute' => 'a:2:{s:9:"range_min";i:50;s:9:"range_max";i:1000;}',
            ],
            [
                'prize_name' => 'Bonus',
                'prize_attribute' => 'a:2:{s:9:"range_min";i:50;s:9:"range_max";i:1000;}',
            ],
            [
                'prize_name' => 'Subject',
                'prize_attribute' => 'a:1:{s:10:"table_name";s:11:"prize_items";}',
            ]
        ])->execute();


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220704_105135_insert_prizes_rows cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220704_105135_insert_prizes_rows cannot be reverted.\n";

        return false;
    }
    */
}
