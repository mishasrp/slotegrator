<?php

use yii\db\Migration;

/**
 * Class m220704_121504_insert_prize_items_rows
 */
class m220704_121504_insert_prize_items_rows extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Yii::$app->db->createCommand()->batchInsert('prize_items', ['name'], [
            [
                'name' => 'Car'
            ],
            [
                'name' => 'Telephone'
            ],
            [
                'prize_name' => 'Television'
            ],
            [
                'prize_name' => 'Kettle'
            ]
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220704_121504_insert_prize_items_rows cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220704_121504_insert_prize_items_rows cannot be reverted.\n";

        return false;
    }
    */
}
