<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prize_items}}`.
 */
class m220704_121246_create_prize_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('prize_items', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique()
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%prize_items}}');
    }
}
