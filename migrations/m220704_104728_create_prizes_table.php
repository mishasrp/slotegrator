<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prizes}}`.
 */
class m220704_104728_create_prizes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('prizes', [
            'id' => $this->primaryKey(),
            'prize_name' => $this->string()->notNull()->unique(),
            'prize_attribute' => $this->string()
        ], $tableOptions);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('prizes');
    }
}
