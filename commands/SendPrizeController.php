<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\UserPrize;
use app\models\Bank;
use yii\console\Controller;
use yii\console\ExitCode;

class SendPrizeController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */

    public function actionPayment($count = 0)
    {
        $count = (int)$count;
        if ($count <= 0) {
            echo "Please enter a number greater than 0\n\r";
            return ExitCode::OK;
        }

        $userPrize = UserPrize::getUserMoney($count);
        $prizeArray = [];
        foreach ($userPrize as $row) {
            $prizeArray[] = $row["id"];
            Bank::send($row["id"], $row["value"]);
        }
        UserPrize::updateAll(['issued' => UserPrize::ISSUED_ACTIVED], ['in', 'id', $prizeArray]);
        return ExitCode::OK;
    }

    public function actionConvert2Bonus($user_id)
    {
        $result = false;
        $userPrize = UserPrize::getUserMoneyById($user_id);
        $prizeArray = [];
        foreach ($userPrize as $row) {
            $prizeArray[] = $row["id"];
        }
        UserPrize::updateAll(['issued' => UserPrize::ISSUED_ACTIVED, 'prize_id' => 2], ['in', 'id', $prizeArray]);

        $count = UserPrize::getUserMoneyByIdCount($user_id);
        echo "Transferred to bonus account count: " . sizeof($prizeArray);
        if ($count == 0) $result = true;
        return $result;
    }
}
