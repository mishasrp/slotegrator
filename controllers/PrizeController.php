<?php

namespace app\controllers;

use Yii;

use app\models\Prize;
use app\models\PrizeItem;
use app\models\UserPrize;
use app\models\AttributeType;
use yii\web\Controller;


class PrizeController extends Controller
{

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        } else {
            return $this->render('prize');
        }
    }

    public function actionGet()
    {
        if (!Yii::$app->user->isGuest) {
            $request = Yii::$app->request;
            if ($request->isPost) {
                $data = $this->getPrize();

                $userPrize = new UserPrize();
                $userPrize->user_id = $data["user_id"];
                $userPrize->prize_id = $data["prize_id"];
                $userPrize->prize_item_id = $data["prize_item_id"];
                $userPrize->value = $data["value"];
                $userPrize->save();

                return $this->render('win', compact('data'));
            } else {
                return $this->goHome();
            }
        } else {
            return $this->goHome();
        }
    }

    private function getAttributeType($prize_attribute)
    {
        if (isset($prize_attribute["range_min"]) && isset($prize_attribute["range_max"])) {
            return AttributeType::RANGE;
        } else if (isset($prize_attribute["table_name"])) {
            return AttributeType::TABLE;
        }
    }

    private function getRandValue($prize_attribute)
    {
        return $this->getRandom($prize_attribute["range_min"], $prize_attribute["range_max"]);
    }

    private function getRandom($min, $max)
    {
        return rand($min, $max);
    }

    private function getItem($prizeAttribute)
    {
        $data = [
            'prize_item_id' => NULL,
            'prize_item_name' => NULL
        ];
        if (isset($prizeAttribute["table_name"])) {
            if ($prizeAttribute["table_name"] == "prize_items") {
                $prizeItem = PrizeItem::getRandomItem();

                $data['prize_item_id'] = $prizeItem->id;
                $data['prize_item_name'] = $prizeItem->name;
            }
        }
        return $data;
    }

    private function getPrize()
    {
        $data = [
            'user_id' => Yii::$app->user->getIdentity()->getId(),
            'prize_id' => NULL,
            'prize_name' => NULL,
            'prize_item_id' => NULL,
            'prize_item_name' => NULL,
            'value' => 0
        ];

        $prize = Prize::getRandomPrize();

        $prize->prize_attribute = unserialize($prize->prize_attribute);

        $data['prize_id'] = $prize->id;
        $data['prize_name'] = $prize->prize_name;

        switch ($this->getAttributeType($prize->prize_attribute)) {
            case AttributeType::RANGE:
            {
                $data["value"] = $this->getRandValue($prize->prize_attribute);
            }
            case AttributeType::TABLE:
            {
                $data = array_merge($data, $this->getItem($prize->prize_attribute));
            }
        }
        return $data;
    }
}