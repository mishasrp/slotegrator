<?php


namespace app\models;


class AttributeType
{
    const RANGE = 0;
    const TABLE = 1;
}