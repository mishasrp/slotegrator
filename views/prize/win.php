<?php

/** @var yii\web\View $this */

$this->title = 'Win Prize';
?>
<div class="site-index">
    <h3>Your winnings</h3>

    <h4><?= $data["prize_name"]; ?> <?= (!empty($data["value"])) ? "- $" . $data["value"] : "" ?> <?= (!empty($data["prize_item_name"])) ? "- " . $data["prize_item_name"] : "" ?></h4>
</div>