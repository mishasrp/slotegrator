<?php

/** @var yii\web\View $this */

$this->title = 'Get Prize';
?>
<div class="site-index">
    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Get Prize!</h1>
        <form method="post" action="/prize/get">
            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <button type="submit" class="btn btn-primary">Забрать приз!</button>
        </form>
    </div>
</div>
