<?php
namespace commands;

use app\commands\SendPrizeController;
use app\models\UserPrize;

class SendPrizeTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testConvert()
    {
        $user_id = 1;
        $this->assertTrue(SendPrizeController::actionConvert2Bonus($user_id));
    }
}