<?php

namespace tests\unit\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{


    public function testFindUserByEmail()
    {
        expect_that($user = User::findByEmail('test@test.ua'));
        expect_not(User::findByEmail('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByEmail('test@test.ua');

        expect_that($user->validatePassword('12345'));
        expect_not($user->validatePassword('123456'));        
    }

}
